/**
 * 指令入口模块
 */
import Vue from 'vue'

// 加载notify模块
import notify from '@/js/util/notify'

// 加载正则表达式模块
import {
  rChinese,
  rIdentity,
  rPhone,
  rEmail
} from '@/js/util/re'

import {
  APPLICANT_NAME_EMPTY_ERROR,
  APPLICANT_IDENTITY_EMPTY_ERROR,
  APPLICANT_EMAIL_EMPTY_ERROR,
  APPLICANT_PHONE_EMPTY_ERROR,

  INSURED_NAME_EMPTY_ERROR,
  INSURED_IDENTITY_EMPTY_ERROR,

  APPLICANT_NAME_ERROR,
  APPLICANT_IDENTITY_ERROR,
  APPLICANT_EMAIL_ERROR,
  APPLICANT_PHONE_ERROR,

  INSURED_NAME_ERROR,
  INSURED_IDENTITY_ERROR
} from '@/js/const'

let validationHash = {
  'applicant_name': {
    empty: {
      msg: APPLICANT_NAME_EMPTY_ERROR
    },
    strict: {
      pattern: rChinese,
      msg: APPLICANT_NAME_ERROR
    }
  },

  'applicant_identity': {
    empty: {
      msg: APPLICANT_IDENTITY_EMPTY_ERROR
    },
    strict: {
      pattern: rIdentity,
      msg: APPLICANT_IDENTITY_ERROR
    }
  },

  'applicant_phone': {
    empty: {
      msg: APPLICANT_PHONE_EMPTY_ERROR
    },
    strict: {
      pattern: rPhone,
      msg: APPLICANT_PHONE_ERROR
    }
  },

  'applicant_email': {
    empty: {
      msg: APPLICANT_EMAIL_EMPTY_ERROR
    },
    strict: {
      pattern: rEmail,
      msg: APPLICANT_EMAIL_ERROR
    }
  },

  'insured_name': {
    empty: {
      msg: INSURED_NAME_EMPTY_ERROR
    },
    strict: {
      pattern: rChinese,
      msg: INSURED_NAME_ERROR
    }
  },

  'insured_identity': {
    empty: {
      msg: INSURED_IDENTITY_EMPTY_ERROR
    },
    strict: {
      pattern: rIdentity,
      msg: INSURED_IDENTITY_ERROR
    }
  }
}

// 注册一个全局自定义指令 `v-validate`
Vue.directive('validate', {
  // 当被绑定的元素插入到 DOM 中时……
  inserted: function (el) {
    el.addEventListener('blur', function () {
      // 获取data-ref属性
      let ref = el.getAttribute('data-ref')
      let val = el.value

      console.log(ref, val)

      // 验证策略对象
      let strategy = validationHash[ref]

      if (strategy &&
        strategy.empty &&
        val === '') {
        notify(strategy.empty.msg)
        return
      }

      if (strategy &&
        strategy.strict &&
        !strategy.strict.pattern.test(val)
      ) {
        notify(strategy.strict.msg)
      }

      // Event
    })
  }
})
