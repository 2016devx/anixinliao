import axios from 'axios'

import queryRisk from './queryRisk'
import queryPremiumCal from './queryPremiumCal'
import queryProfession from './queryProfession'
import queryInsure from './queryInsure'
import queryInsuranceDetail from './queryInsuranceDetail'

import notify from '@/js/util/notify'

import {
  SERVER_ERROR
} from '@/js/const'

import store from '@/store'

// ===== 配置API默认信息 =====
// 接口地址
axios.defaults.baseURL = 'http://api.xjt365.cn/v1'
axios.defaults.headers.post['Content-type'] = 'application/json;charset=utf8'

// 拦截器配置
axios.interceptors.request.use(config => {
  store.commit('toggleAjaxLoadingStatus', true)
  return config
}, err => {
  store.commit('toggleAjaxLoadingStatus', false)
  Promise.reject(err)
})

axios.interceptors.response.use(config => {
  store.commit('toggleAjaxLoadingStatus', false)
  return config
}, err => {
  store.commit('toggleAjaxLoadingStatus', false)
  Promise.reject(err)
})

/**
 * Ajax获取保费
 * @param {*} state Vuex状态实例
 * @param {*} comp 组件实例
 *
 * @note
 * 起因：DONT REPEAT YOURSELF
 */
var getPremium = (state, comp) => {
  queryPremiumCal({
    birthday: state.birthday,
    social: state.social,
    planno: state.planno
  })
    .then((response) => {
      console.log('@api保费计算...')
      var $response = response.data.response
      var $h = $response.head
      var $errcode = $h.errcode
      if ($errcode === '00000') {
        var d = $response.body.data
        console.log('响应数据:', d)

        // 设置业务id编号
        comp.$store.commit('changeTransactionId', d.transactionid)

        // 设置保费
        comp.$store.commit('changePremium', d.premium)

        // comp.$store.commit('changeStartDate', d.insure_start_date)
        // comp.$store.commit('changeEndDate', d.insure_end_date)

        comp.$store.commit('changePremiumSuccessful', true)
      } else {
        comp.$store.commit('changePremiumSuccessful', false)

        notify(
          $h.message[0].validate,
          'error',
          5)
      }
      // 无论加载成功或者失败，统一设置为false
      comp.initLoading = false
    }).catch(() => {
      // comp.$store.commit('changePremiumSuccessful', false)
      notify(
        SERVER_ERROR,
        'error',
        5)
      comp.initLoading = false
    })
}

export {
  queryRisk
  , queryPremiumCal
  , queryProfession
  , getPremium
  , queryInsure
  , queryInsuranceDetail
}
