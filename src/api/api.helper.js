import {
  extend
} from '@/js/util/index'

import axios from 'axios'

const INSURAER = 'CHINALIFE'

// 查询参数，方便区分不同公司的请求
const suffix = 'company=senrui&type=wd'

var ajax = (url, params) => axios.post(url, params)

// 默认请求体JS对象表示
var defaultRequestObject = {
  'request': {
    'head': {
      'account': '',
      'businessid': '',
      'sendtime': '',
      'sign': '',
      'callback': '',
      'custom': '',
      'request_type': ''
    },
    'body': {
    }
  }
}

// 请求体构建器
var requestBodyBuilder = (options) => {
  var obj = extend(
    true, // 深复制
    {},
    defaultRequestObject
    , options)

  return obj
}

export {
  requestBodyBuilder,
  INSURAER,
  ajax,
  suffix
}
