import {
  requestBodyBuilder,
  INSURAER,
  ajax,
  suffix
} from './api.helper'

var queryProfession = (options) => {
  options = options || {}
  var data = requestBodyBuilder({
    request: {
      head: {
        request_type: 'QUERY_PROFRESSION'
      },
      body: {
        insurer: INSURAER,
        parentno: options.parentno || '0'
      }
    }
  })

  return ajax(`/life/profession?${suffix}`, data)
}

export default queryProfession
