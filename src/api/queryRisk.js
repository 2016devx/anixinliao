import {
  requestBodyBuilder,
  INSURAER,
  ajax,
  suffix
} from './api.helper'

var queryRisk = () => {
  var data = requestBodyBuilder({
    request: {
      head: {
        request_type: 'QUERY_RISKS'
      },
      body: {
        insurer: INSURAER
      }
    }
  })

  return ajax(`/life/risks?${suffix}`, data)
}

export default queryRisk
