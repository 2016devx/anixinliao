import {
  requestBodyBuilder,
  INSURAER,
  ajax,
  suffix
} from './api.helper'

var queryPremiumCal = (options) => {
  var data = requestBodyBuilder({
    request: {
      head: {
        request_type: 'QUERY_PREMIUM_CAL'
      },
      body: {
        birthday: options.birthday,
        social: options.social,
        planno: options.planno,
        insurer: INSURAER
      }
    }
  })

  return ajax(`/life/premiumcal?${suffix}`, data)
}

export default queryPremiumCal
