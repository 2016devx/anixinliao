import {
  requestBodyBuilder,
  INSURAER,
  ajax,
  suffix
} from './api.helper'

var queryInsuranceDetail = (options) => {
  options = options || {}
  var data = requestBodyBuilder({
    request: {
      head: {
        request_type: 'QUERY_INSURANCE_INFO'
      },
      body: {
        insurer: INSURAER,
        insuranceno: options.insuranceno
      }
    }
  })

  return ajax(`/life/insureinfo?${suffix}`, data)
}

export default queryInsuranceDetail
