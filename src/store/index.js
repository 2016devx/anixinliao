import Vue from 'vue'
import Vuex from 'vuex'

import {
  normalizeDate
} from '@/js/util'

import {
  getPremium
} from '@/api'

Vue.use(Vuex)

// Application Store
const store = new Vuex.Store({
  state: {
    // Ajax加载器状态
    ajaxLoading: false,

    // 套餐编号
    planno: 'PLANNO',
    // 投保人出生日期
    birthday: 'XXXX-XX-XX',
    // 用于限制日历插件可显示的最小日期(可投保的最早日期)
    min_date: 'XXXX-XX-XX',
    // 用于限制日历插件可显示的最大日期(可投保的最晚日期)
    max_date: 'XXXX-XX-XX',

    product_instruction: '',
    extended_medical: '',
    abatement: '',
    hospital: '',

    // 健康告知书
    health: '',

    // // 起保日期
    // start_date: 'XXXX-XX-XX',
    // // 终保日期
    // end_date: 'XXXX-XX-XX',

    // '0'代表无社保，'1'代表有社保
    social: '0',
    // 保费
    premium: 0,

    // 业务id编号(保费计算接口返回)
    transactionid: '',

    // // 投保前健康告知
    // health: '',
    // // 客户告知
    client_nft: '',

    // 申请人(投保人)与被保人两者的关系:
    // 0: 本人
    // 1: 配偶
    // 2: 子女
    // 3: 父母
    relative: '0',

    // 申请人(投保人)姓名
    applicant_name: '',

    // 申请人(投保人)身份证
    applicant_identity: '',

    // 申请人(投保人)手机号码
    applicant_phone: '',

    // 申请人(投保人)联系的邮箱
    applicant_email: '',

    // 被保人姓名(两者关系不是本人的请传入)
    insured_name: '',

    // 被保人身份证(两者关系不是本人的请传入)
    insured_identity: '',

    // 被保人职业编号
    insured_profession: '',

    // 被保人职业名称
    insured_profession_name: '',

    // 投保单号(由投保接口返回)
    insuranceno: '',

    relationship: '本人',

    // 保单详情信息
    insurance_detail: null,

    // ****** 与业务无关 **********
    premiumSuccessful: false
  },

  mutations: {
    changePremiumSuccessful (state, premiumSuccessful) {
      state.premiumSuccessful = premiumSuccessful
    },

    // 设置一般医疗保险金条款说明
    changeClientNft (state, clientNft) {
      state.client_nft = clientNft
    },

    changeProductInstruction (state, productInstruction) {
      state.product_instruction = productInstruction
    },

    // 设置扩展6种重大疾病条款说明
    changeExtendedMedical (state, extendedMedical) {
      state.extended_medical = extendedMedical
    },

    // 设置年度免赔额条款说明
    changeAbatement (state, abatement) {
      state.abatement = abatement
    },

    // 设置医院类型
    changeHospital (state, hospital) {
      state.hospital = hospital
    },

    changeHealth (state, health) {
      state.health = health
    },

    /**
     * 更改“关系”字段
     */
    changeRelationship (state, relationship) {
      state.relationship = relationship
    },

    /**
     * 切换Ajax加载器状态
     * @param {Object} state Vuex状态
     * @param {Boolean} status 加载状态
     */
    toggleAjaxLoadingStatus (state, status) {
      state.ajaxLoading = status
    },

    changeInsuranceDetail (state, d) {
      state.insurance_detail = d
    },

    // 更改关系
    changeRelative (state, relative) {
      state.relative = relative
    },

    // 更改申请人姓名
    changeApplicantName (state, applicantName) {
      state.applicant_name = applicantName
    },

    // 更改申请人身份证号码
    changeApplicantIdentity (state, applicantIdentity) {
      state.applicant_identity = applicantIdentity
    },

    // 更改申请人手机号码
    changeApplicantPhone (state, applicantPhone) {
      state.applicant_phone = applicantPhone
    },

    // 更改申请人邮箱
    changeApplicantEmail (state, applicantEmail) {
      state.applicant_email = applicantEmail
    },

    // 更改被保人姓名
    changeInsuredName (state, insuredName) {
      state.insured_name = insuredName
    },

    // 更改被保人身份证号码
    changeInsuredIdentity (state, InsuredIdentity) {
      state.insured_identity = InsuredIdentity
    },

    // 更改被保人职业编号
    changeInsuredProfession (state, no) {
      state.insured_profession = no
    },

    // 更改被保人职业名称
    changeInsuredProfessionName (state, name) {
      state.insured_profession_name = name
    },

    changePremium (state, premium) {
      state.premium = premium
    },

    changePlanNo (state, planno) {
      state.planno = planno
    },

    selectBirthday (state, date) {
      state.birthday = normalizeDate(date)
    },

    // // 提供以下两个方法目地是避免用户出现跨日购买的情况
    // changeStartDate (state, startDate) {
    //   state.start_date = startDate
    // },

    // changeEndDate (state, endDate) {
    //   state.end_date = endDate
    // },

    changeMinDate (state, minDate) {
      state.min_date = minDate
    },

    changeMaxDate (state, maxDate) {
      state.max_date = maxDate
    },

    changeSocialSecurity (state, social) {
      state.social = social
    },

    // 切换社保购买状态
    toggleSocialSecurity (state, buyStatus) {
      if (buyStatus === 'YES') {
        state.social = '1'
      } else if (buyStatus === 'NO') {
        state.social = '0'
      }
    },

    // View#queryJobClass
    changeList (state, list) {
      state.list = list
    },

    changeTransactionId (state, transactionid) {
      state.transactionid = transactionid
    },

    changeInsuranceNo (state, insuranceno) {
      state.insuranceno = insuranceno
    }
  },

  actions: {
    selectBirthday ({ commit, state }) {
      // 获取组件实例
      var comp = arguments[1].comp

      comp.$calendar.show({
        date: state.birthday,
        startTime: state.min_date,
        endTime: state.max_date,
        onOk: (date) => {
          commit('selectBirthday', date)
          comp.initLoading = true
          getPremium(state, comp)
        }
      })
    },

    // 请求保费计算
    requestPremiumCal ({ commit, state }) {
      // 获取组件实例
      var comp = arguments[1].comp
      comp.initLoading = true

      getPremium(state, comp)
    }
  }
})

export default store
