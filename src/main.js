// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

// 加载Store模块
import store from '@/store/index'

// // [错误跟踪](https://sentry.io/welcome/)
// import Raven from 'raven-js'
// import RavenVue from 'raven-js/plugins/vue'

// Raven
//   .config('https://fe1e70bd428b4c4d972a6a6fc64c350a@sentry.io/252535')
//   .addPlugin(RavenVue, Vue)
//   .install()

// import '@/directive/index'

// Vue配置
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
