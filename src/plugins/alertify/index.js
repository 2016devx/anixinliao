// alertify配置
import alertify from './alertify'

alertify.set('notifier', 'position', 'top-center')

export default alertify
