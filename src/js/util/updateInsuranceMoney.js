/**
 * 套餐保额更新模块
 * @date 2017-11-20
 */
import planFactory from './plan.factory'

/**
 * 更新组件$refs中的保额
 * @param {Object} componentRefs 组件的$refs属性
 * @param {String} planno 套餐代号
 */
var updateInsuranceMoney = (componentRefs, planno) => {
  var obj = planFactory(planno)

  // [Vue $ref](https://cn.vuejs.org/v2/api/#ref)
  // 使用$refs来引用DOM
  // $refs 也不是响应式的，你不应该试图用它在模板中做数据绑定
  componentRefs.commonMoney.textContent = obj.commonMedicalTraementInsuranceMoney
  componentRefs.extendedMoney.textContent = obj.extendedMedicalTraementInsuranceMoney

  // // 检测是为了避免操作异常
  // if (componentRefs && componentRefs.extraMoney) {
  //   componentRefs.extraMoney.textContent = obj.extraMedicalTraementInsuranceMoney
  // }
}

export default updateInsuranceMoney
