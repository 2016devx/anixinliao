/**
 * 险别说明工厂模块
 * @date 2017-11-20
 */
import {
  productInstruction,
  extendedMedical,
  extraBorrowed,
  franchiseOfYear,
  hospital
} from '@/raw'

var instructionFactory = (refName) => {
  switch (refName) {
    case 'commonMoney':
      return productInstruction
    case 'extendedMoney':
      return extendedMedical
    case 'extraMoney':
      return extraBorrowed
    case 'franchiseOfYear':
      return franchiseOfYear
    case 'hospital':
      return hospital
  }
}

export default instructionFactory
