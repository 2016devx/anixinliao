import store from '@/store'
// import notify from '@/js/util/notify'

/**
 * 同步Vuex应用程序状态
 * @param {Store} store Vuex实例
 * @param {String} actionName 动作名称
 * @param {Any} value 要同步的状态值
 */
var syncState = (store, actionName, value) => store.commit(actionName, value)

/**
 * 处理表单输入
 *
 * @param {String}  inputValue 当前表单项所输入的值
 * @param {String}  actionName Vuex action名称
 */
var handleInput = (
  eventTarget,
  actionName
) => {
  // 当前表单项所输入的值
  var v = eventTarget.value.trim()
  // eventTarget.blur()
  // 触发commit来同步状态
  syncState(store, actionName, v)
}

export default handleInput
