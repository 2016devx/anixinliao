/**
 * 套餐工厂模块
 * @date 2017-11-20
 */
var planFactory = (planno) => {
  switch (planno) {
    case 'LP0001' :
      return {
        // 一般医疗保险金
        commonMedicalTraementInsuranceMoney: '100万',
        // 扩展医疗保险金
        extendedMedicalTraementInsuranceMoney: '100万',
        // 额外医疗保险金
        extraMedicalTraementInsuranceMoney: '无'
      }
    case 'LP0002':
      return {
        // 一般医疗保险金
        commonMedicalTraementInsuranceMoney: '100万',
        // 扩展医疗保险金
        extendedMedicalTraementInsuranceMoney: '100万',
        // 额外医疗保险金
        extraMedicalTraementInsuranceMoney: '1万'
      }
    case 'LP0003':
      return {
        // 一般医疗保险金
        commonMedicalTraementInsuranceMoney: '300万',
        // 扩展医疗保险金
        extendedMedicalTraementInsuranceMoney: '300万',
        // 额外医疗保险金
        extraMedicalTraementInsuranceMoney: '无'
      }
    case 'LP0004':
      return {
        // 一般医疗保险金
        commonMedicalTraementInsuranceMoney: '300万',
        // 扩展医疗保险金
        extendedMedicalTraementInsuranceMoney: '300万',
        // 额外医疗保险金
        extraMedicalTraementInsuranceMoney: '1万'
      }
  }
}

export default planFactory
