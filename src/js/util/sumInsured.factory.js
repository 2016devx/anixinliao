/**
 * 保额工厂模块
 * @date 2017-11-22
 */
var sumInsuredFactory = (planno) => {
  switch (planno) {
    case 'LP0001':
      return 200
    case 'LP0002':
      return 201
    case 'LP0003':
      return 600
    case 'LP0004':
      return 601
  }
}

export default sumInsuredFactory
