/* eslint-disable */
// Work with page objects
// http://nightwatchjs.org/guide/#page-objects
let browser

const NAME_ERROR = '投保人姓名不能为空'

module.exports = {
  'chinalife@step-1': function (client) {
    browser = client.page.chinalife()
    browser.navigate()

    // 等待页面显示（因为页面需要JS来渲染）
    browser.waitForElementVisible('body', 5000)

    // browser.assert.title('百万安心疗')

    // 暂停方便Ajax发起请求
    client.pause(5000)

    // 预期会出现报价的容器
    browser.waitForElementVisible('@premiumContainer', 10000)

    browser.assert.containsText('@premiumContainer', '215元')

    client.pause(2000)

    browser.assert.cssClassPresent('@firstPlanItem', 'it-item-label__active')
  },

  'chinalife@step-2': function (client) {
    // 测试套餐切换功能
    browser
			.click('@lastPlanItem') // 点击最后一个套餐
		
		client.pause(5000)

    browser.waitForElementVisible('@premiumContainer', 2000)
      .assert.containsText('@premiumContainer', '254元')

    client.pause(2000)

    browser
      .click('@proposal') // 点击立即投保按钮
      .waitForElementVisible('@healthAnnouncement', 5000) // 等待健康告知书页面展示
      .assert.urlContains('#/healthAnnouncement') // 判断hash是否发生变化
  },

  'chinalife@step-3': function (client) {
    browser
      .click('@btnApprove') // 点击符合按钮
    // Pause the client
    client.pause(2000)

    browser.waitForElementVisible('@insuranceInfo', 5000) // 等待信息填写页面显示
      .assert.urlContains('#/insuranceInfo') // 判断hash是否发生变化
  },

  // 测试投保信息页面不输入任何信息
  'chinalife@step-4': function (client) {
    browser
      .click('@btnSubmit') // 点击提交按钮
      .waitForElementVisible('@alertifyMessageContainer', 5000) // 等待信息填写页面显示
      .assert.containsText('@alertifyMessageContainer', NAME_ERROR)
      .assert.urlContains('#/insuranceInfo') // 判断hash是否发生变化

    // Pause the client
    client.pause(2000)
  },

  // 测试投保页面输入正确的信息
  'chinalife@step-5': function (client) {
    // 填寫基本信息
    browser.setValue('@applicantName', '张三')
    browser.setValue('@applicantIdentity', '440782199302233332')
    browser.setValue('@applicantPhone', '13168381876')
    browser.setValue('@applicantEmail', '942850532@qq.com')
  },

  // 职业查询功能检测
  'chinalife@step-6': function (client) {
    browser.click('@btnQueryJob', function () {
      // client.pause(5000)
      browser.waitForElementVisible('@queryJobPage', 10000)
      browser.waitForElementVisible('@firstJobItem', 5000)
      browser.click('@firstJobItem') // 点击列表中第一個Item
      client.pause(5000)
      browser.waitForElementVisible('@firstJobItem', 5000)
      browser.click('@firstJobItem') // 点击列表中第一個Item
      client.pause(5000)
      browser.waitForElementVisible('@firstJobItem', 5000)
      browser.click('@firstJobItem') // 点击列表中第一個Item
      client.pause(5000)
      // 点击“提交”按钮
      browser.click('@btnConfirm')
      client.pause(5000)
      browser.waitForElementVisible('@insuranceInfo', 5000) // 等待信息填写页面显示
      browser.assert.urlContains('#/insuranceInfo')
    })
  },

  'chinalife@step-7': function (client) {
    browser
      .click('@btnSubmit') // 点击提交按钮

    client.pause(5000)
		browser.waitForElementVisible('@clientAnnouncement', 5000)
		
		// 点击确认投保
		browser.click('@btnConfirmProposal')
		browser.waitForElementVisible('@btnViewProposalInfo', 5000)

		// 点击查看投保信息
		browser.click('@btnViewProposalInfo')
		
    browser.waitForElementVisible('@insuranceDetail', 5000) // // 等待投保详情页面显示
    browser.assert.urlContains('#/insuranceDetail')
  },

  'chinalife@step-last': function (client) {
    // 关闭客户端
    client.end()
  }
}
