/* eslint-disable */

module.exports = {
  url: 'http://localhost:8080',
  // url: 'http://api.bxh365.cn/view/v1/life/index#/',
  elements: {
    // 报价容器
    premiumContainer: {
      selector: 'div.page-entry > div.base-info-wrapper > div:nth-child(5) > div'
    },

    // 套餐item
    firstPlanItem: {
      selector: 'div.page-entry > nav > ul > li:nth-child(1) > span'
    },

    lastPlanItem: {
      selector: 'div.page-entry > nav > ul > li:nth-child(4) > span'
    },

    proposal: {
      // 点击投保按钮
      selector: 'div.page-entry > div.fixed-bar-wrapper > div > div.proposal-wrapper.flex-1 > a'
    },

    healthAnnouncement: {
      // 健康告知书页面容器
      selector: 'div.page-health-announcement'
    },

    btnApprove: {
      selector: '.approve-btn'
    },

    insuranceInfo: {
      selector: 'div.page-insurance-info'
    },

    btnSubmit: {
      selector: '.submit-btn'
    },

    alertifyMessageContainer: {
      selector: '.ajs-error'
    },

    applicantName: {
      selector: 'div.page-insurance-info > div.page-content > div.insurer-info-wrapper > div:nth-child(2) > div.bi-item-extra.flex-1 > input'
    },

    applicantIdentity: {
      selector: 'div.page-insurance-info > div.page-content > div.insurer-info-wrapper > div:nth-child(3) > div.bi-item-extra.flex-1 > input'
    },

    applicantPhone: {
      selector: 'div.page-insurance-info > div.page-content > div.insurer-info-wrapper > div:nth-child(4) > div.bi-item-extra.flex-1 > input'
    },

    applicantEmail: {
      selector: 'div.page-insurance-info > div.page-content > div.insurer-info-wrapper > div:nth-child(5) > div.bi-item-extra.flex-1 > input'
    },

    btnQueryJob: {
      selector: '.btn-queryjob'
    },

    queryJobPage: {
      selector: '.page-query-jobclass'
    },

    firstJobItem: {
      selector: 'div.page-query-jobclass > ul > li:nth-child(1)'
    },

    // 职业等级查询页面确定按钮
    btnConfirm: {
      selector: '.btn-confirm'
		},
		
		clientAnnouncement: {
			selector: '.page-client-announcement'
		},

		btnConfirmProposal: {
			selector: '#J-confirm-proposal'
		},

		btnViewProposalInfo: {
			selector: '#J-view-proposal-info'
		},

    insuranceDetail: {
      selector: '.page-insurance-detail'
		}
  }
}
